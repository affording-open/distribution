Transparent secure digital distribution
=======================================

Status: drafting

Distribution transparency allows a person to receive a distribution of
software or data including derivatives and know if anyone shared any
information of a specific type about it like knowledge of a security
defect. Thus it affords one to build and deliver an OS securely.

This has a special focus on operating system (OS) software
distributions for personal computers, but works equally well for any
data.

This should outline how to provide evidence for security by design and
construction instead of only absence of evidence for insecurity. It
can not obtain a 100% guarantee, but it should be falsifiable. If a
risk is said to not need a defense with the justification that it is
unlikely, it should include a way to measure that it doesn't happen
and that the probability of the risk didn't change.

Threat model
------------

While different people have different threat models for their
computers, this targets a widely used general purpose OS. With a
widely deployed OS, some people will use it in much more security
critical situations. Thus it needs to meet those higher security
requirements even if the majority may not need it right now.

Attackers...
* have interest in the distribution being exploitable by them if users
  of the distribution are interesting targets.
* have interest in gaining long lasting access to the related
  infrastructure (e.g. individual developer machines, source storage,
  version control servers, CI, build servers, mirrors, etc) if that
  can be used to attack users of the distribution.
* can buy, steal or develop zero-day exploits if there exists security
  bugs that allow this.
* can employ people to create custom engineered software or hardware
  to exploit one target once.
* can hire people with top qualifications or otherwise ensure someone
  becomes their agent directly or indirectly, even without their
  knowledge.
* have an interest not only in some people in an organisation that
  works on a distribution as even people without direct access can
  become a stepping stone (e.g. a people manager to influencing hiring
  decisions to position their agent; someone in an adjacent office
  near more interesting people or hardware to collect information; an
  investor to buy some specific influence).
* can sustain and not use their agents to work for longer times on the
  distribution or otherwise an involved organisation until they are
  trusted enough to only then subvert it (sleeper agents).
* can influence law enforcement, courts and law makers. However this
  is somewhat, but not fully restricted by jurisdictional separation
  like nation state borders. This is not restricted to nation state
  actors due to various ways that non-state actors are afforded power
  over state institutions that may be explicitly written into law,
  implicit side effects of applied law or due to corruption.

However attackers with much fewer capabilities are commonly successful
because often even basic defenses were neglected.

TODO improve the list below

List of example successful attack outcomes:
* compromise the communication about a security bug
* compromise the ability of the normal maintainer or an unrelated
  person to make and release a fix for a security bug
* introduce a change with a security bug in source code management
  system through the normal process like getting it merged on Github
* compromise the review process to merge a change that wouldn't have
  been accepted
* introduce a malicious change in intermediate source artefact like
  tarball
* introduce a malicious change in a binary artefact
* compromise a build system
* compromise the CI
* compromise the supply chain, like a binary or downstream source used
  in the build
* steal a signing key or get a malicious artefact signed without
  stealing the key
* impact the availability of artefacts for a user
* make a user act as if no update was available even when there is one
* make a user accept an older version even if a newer one is available

(This document does not consider sources that must be kept private and
only works for fully public information.)

Requirements and possible solutions
-----------------------------------

The only idea to counter the above threats is to not rely on any
centralisation to provide security. Thus trying to create a mechanism
to make it easier for you to cooperate with many people and not place
an unrealistic burden on fewer people. (If you have other solutions,
please file a ticket.) This has the additional advantage that it
protects the people working on the OS.

Secure here does not mean merely from the point of view of one person,
because that would afford less security to that person. One of the
main mechanisms suggested to afford that security is via transparency,
which here means a way to detect by people when their view on
something is different. It does not require global consistency, but
inconsistency detection. Transparency as used here is related to
non-repudiation.

It is of importance that there are currently many people and
organisations involved in the software and hardware that make up
common computers, so security needs to scale.

Providing security by default and without cost to usability so that it
provides features and is otherwise invisible is a good path to have it
when it is needed. Or to quote AviD: Security at the expense of
usability comes at the expense of security.

To achieve secure distribution, the availability and integrity of all
its steps are necessary:
* source creation
* communication
* source control
* source review
* continuous integration (CI)
* build
* release
* distribution to users

While most of the information is needed to be public to achieve
security, for certain private information confidentiality is needed to
reduce risk. This also means that it needs to be supported to correct
the decision on whether something needs to be public or private.

Some more details:

* Availability
  * Have a full copy, by easy automated mirroring. Download and build
    need to be separated. Enumerating what to mirror, at least
    coarsely, needs to be easy, safe without a sandbox. Content needs
    to be authenticated (instead of authenticating the transport or
    server). Using and switching mirrors needs to be easy. (E.g. the
    local build and CI infrastructure needs to use the chosen mirrors;
    a mirror of the documentation, which is a build artefact, needs to
    be usable in a browser as it is at the original URL.) Things that
    need to be mirrored:
	* source control (e.g. git repo, see loose history as under
      integrity, git push log, review annotations)
	* task/bug trackers
	* code review data
	* mailing lists
	* forums
	* wikis
	* public chat logs
	* CI logs and artifacts
	* build logs and artifacts
	* release related data
  * Do not depend on specific infrastructure. This means it needs to
    include the process to set up its own environment and
    infrastructure. This also includes that it needs to be able to
    scale down. While it is fine to use a whole data center to build a
    distribution, it should be possible to run it on one machine
    (though creating multiple concurrent VMs on it may be necessary).
* Integrity
  * Source transparency
    * Try to have as many things that are derived in a build process
      for which binary transparency below applies. All other things
      are source, which can also mean a database entry edited via
      form, an image edited in a pixel editor, and other things, not
      just computer program source code.
	* Sources and binaries should be locally addressable, e.g. content
      addressable.
    * Protected history to be able to roll back from compromise or accident
      * But needs to cope with intentional redactions, by allowing to
        review and potentially trust signed deletion reasons.
    * Needs to check signatures.
	  * Needs self maintained definition of which keys are trusted for
        this project. E.g. in a git repo this could be a .gittrust
        listing key fingerprints.
	  * Needs self maintained identities for key rotation.
	  * Git has both signed commits and signed pushes, also mirror the
        later and check both.
	  * Source control tools should create a key and sign by default.
	    * Web based source control tools should use the Web
          Cryptography API with non-extractable keys to have the
          server not have access to the key. Though it would be
          preferable to not use web based tools for source review.
	  * If not signed, check signature from people working on
        downstream projects or the source host or mirrors.
	* Needs definition of which source branches are only appended to
      (AKA protected) to be able to error (not continue build, but
      continue to mirror) when that fails. Perhaps in .gitprotected .
	  * When source history is rewritten and not declared protected,
        store both, to not loose history. E.g. use
        [git-backup](https://gitlab.com/JanZerebecki/git-backup).
	* Store source changes to a transparency log. See also
      [transparency-log-watcher](https://gitlab.com/JanZerebecki/transparency-log-watcher).
  * Binary transparency
    * Reproducable builds
	  * Depends on builds being hermetic and not needing network
        access (known set of inputs before build starts).
      * Need to be able to test that merely something specific or
        everything reproduces locally, thus needs to work cross
        project, so that one rebuilder can rebuild all necessary
        dependencies.
	  * Tarballs that are distributed as source need to be treated as
        binaries and thus also be reproducible.
	  * CI needs to be reproducible, as test results are a build output.
	  * Some of the artifacts may be useful to store, but are not
        exactly reproducible, which is in that case totally ok, like
        log files. The difference is that these may be inspected by a
        human in case of debugging, but are not used otherwise.
	* Bootstrapable builds
	* Log build artefacts and rebuilds and their failures to a
      transparency log.
  * [Collaborative review in a transparency
    log](https://gitlab.com/affording-open/collaborative-review) to be
    able to coordinate review and to be able to rely on getting
    relevant reviews.
* Confidentiality: While this considers only public distribution,
  there is some meta data that is considered private. This is often a
  difficult decision and difficult to implement. The hope is that
  specific enough workflows implemented carefully can avoid
  unacceptable results which e.g. would have absurd corner cases.
  * Examples of information considered private:
    * IP addresses by default, but a user may my choose differently if
      they prefer speed over privacy. A way to achieve this may be mix
      networks, but this is an area of active research.
	* When someone downloads from the system.
	* Who downloaded which parts.
  * Examples of information considered public:
	* Which pseudonyms publicly interacted with whom on what. (This is
      a big trade off for the case of widely used software in favour
      of being able to make it easier to cooperate on reviews. For
      other applications like a social network, this wouldn't be
      acceptable. Making one off pseudonyms practical is desirable,
      but difficult to sustain once such a system becomes popular.)
	* Maybe how popular a part is, where that doesn't contradict privacy
      goals.
  * Support for keeping information like a security bug report and the
    fix to a few peers until a security embargo times out would be
    helpful.
  * Selective data avoidance needs to be supported. See part about
    redactions under protected history above. This system is only
    supposed to be for specific public data, so it must be able to
    avoid redistributing anything else. This is not strictly a case of
    only confidentiality. On the other hand some will try to misuse
    the concept of or mechanisms for privacy for disrupting security
    or security for disrupting privacy.

* Compartmentalisation
  * Many software applications should not need to be developed with
    the care that is outlined here, to make that possible the OS
    should provide sandboxes and capabilities so that e.g. games do
    neither need to be secure nor can impact the security of the rest
    of the system.
  * This is also important for any infrastructure, so it is important
    to make sure attackers can't e.g. easily move from one developers
    machine to others; use a central server used for maintenance
    (think salt, puppet, etc) to move to all machines in an
    organisation; move from a version control server to the backup
    server.

Comparison of implementations
-----------------------------

This also includes implementations that solve only a part of the
problem.

TODO

Importance in relation to other security improvements
-----------------------------------------------------

The overall security of a system is limited in strength to its weakest
part.

As the most commonly used operating systems do not even try to have
end to end proofs, this is in practice a critical limitation to
distributing a secure one. (See
[seL4](https://en.wikipedia.org/wiki/L4_microkernel_family#High_assurance:_seL4)
for one that does try.)

If you can know faster of more, critical enough security issues than
you can fix them, then any guarantees of reliably transmitting
knowledge of them, fixing them and distributing the fix does not get
you a more secure OS.

However the effect for one installation versus the effect for all
installations are sometimes different. The suggestions in this
document should allow one to avoid the need for relying on just one
person, central machine, or vendor, but instead to rely on multiple
where all of them would need to be compromised to compromise the
distribution.

One might not be able to improve all fronts equally well. Either way,
at some point all are required for a secure distribution.
